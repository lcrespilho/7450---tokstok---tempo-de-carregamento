interface TestOptions {
  browser: puppeteer.Browser; // não funciona. :(
  loads: number;
  trace: boolean;
  har: boolean;
  networkIdleTime: number;
  gtmPreview?: string;
  pageUrl: string;
}