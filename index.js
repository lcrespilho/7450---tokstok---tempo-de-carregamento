/// <reference path="index.d.ts" />

const puppeteer = require('puppeteer');
const { waitForNetworkIdle, getPerformanceMetrics, NETWORK_PRESETS } = require('@lcrespilho/puppeteer-utils');
const PuppeteerHar = require('puppeteer-har');

(async () => {
  const browser = await puppeteer.connect({
    browserURL: 'http://localhost:9222',
    defaultViewport: null,
    devtools: true
  });
  // const browser = await puppeteer.launch({
  //   args: [
  //     '--start-maximized',
  //     '--window-size=1920,1080',
  //     '--remote-debugging-port=9223'
  //   ],
  //   defaultViewport: null,
  //   headless: true,
  //   devtools: false
  // });

  let baseTestsResults = await doTest({
    browser,
    loads: 30,
    trace: false,
    har: false,
    networkIdleTime: 500,
    pageUrl: 'https://www.tokstok.com.br/checkout'
  });
  console.table(baseTestsResults);
  let delayedTagsTestsResults = await doTest({
    browser,
    loads: 30,
    trace: false,
    har: false,
    networkIdleTime: 500,
    gtmPreview: 'https://www.googletagmanager.com/start_preview/gtm?uiv2&id=GTM-P56MFF5&check_preview_status=1&gtm_auth=MENLmtA6eJrpxA0F9ePoXQ&gtm_preview=env-358&gtm_debug=',
    pageUrl: 'https://www.tokstok.com.br/checkout'
  });
  console.table(delayedTagsTestsResults);
  // await browser.close();
  await browser.disconnect();
})();


/**
 * @param {TestOptions} options
 */
async function doTest(options) {
  const metricsList = [];
  const page = await options.browser.newPage();

  let gtmCookies;
  if (options.gtmPreview) {
    // Set GTM Workspace cookie
    await page.goto(options.gtmPreview, { waitUntil: 'networkidle0' });
    await page.waitForNavigation({ waitUntil: 'networkidle0' });
    await page.waitFor(1000);
    gtmCookies = await page.cookies('https://www.googletagmanager.com');
  }
  await page.evaluateOnNewDocument(() => {
    new PerformanceObserver(list => {
      window.longtasks = window.longtasks || [];
      window.longtasks.push(...list.getEntries());
    }).observe({ entryTypes: ['longtask'] });
  });

  // Carregamento de sacrifício
  try {
    await Promise.all([
      waitForNetworkIdle(page, options.networkIdleTime, 0),
      page.goto(options.pageUrl, { waitUntil: 'domcontentloaded' })
    ]);
  } catch (error) {
    console.error('A:', error);
  }

  for (let i = 0; i < options.loads; i++) {
    await page.setCacheEnabled(false);
    await page.setDefaultTimeout(30000);
    await page._client.send('Network.emulateNetworkConditions', NETWORK_PRESETS.TokStok);
    await page.goto('about:blank');
    const t0 = Date.now();
    if (options.trace) {
      await page.tracing.start({ path: `${options.gtmPreview ? 'variant' : 'base'}-trace-${t0}.json`, screenshots: true });
    }
    if (options.har) {
      var har = new PuppeteerHar(page);
      await har.start({ path: `${options.gtmPreview ? 'variant' : 'base'}-${t0}.har` });
    }
    try {
      let [lastLoadedResource] = await Promise.all([
        waitForNetworkIdle(page, options.networkIdleTime, 0),
        page.goto(options.pageUrl, { waitUntil: 'domcontentloaded' })
      ]);
      //console.log('última requisição:', lastLoadedResource);
    } catch (error) {
      console.error('B:', error);
    }
    const finish = Date.now() - t0 - options.networkIdleTime; // simula a marca "Finish" do Chrome
    console.log(`Rodada ${i + 1} de ${options.loads}. Tempo de carregamento: ${finish}`);
    const metrics = await getPerformanceMetrics(page);
    metrics.Finish = finish; // getPerformanceMetrics() não traz essa métrica
    if (options.har) {
      await har.stop();
    }
    if (options.trace) {
      await page.tracing.stop();
    }
    metrics.longtasks = await page.evaluate(() => window.longtasks && window.longtasks.reduce((acc, curr) => acc + curr.duration, 0) || 0);
    metricsList.push(metrics);
  }

  const accumulatedMetrics = metricsList.reduce((acc, curr) => {
    if (JSON.stringify(acc) === '{}') {
      Object.assign(acc, curr);
    } else {
      for (let key in acc) {
        acc[key] += curr[key];
      }
    }
    return acc;
  }, {});
  const averageMetrics = {};
  for (let key in accumulatedMetrics) {
    averageMetrics[key] = Math.round(accumulatedMetrics[key] / options.loads);
  }
  if (options.gtmPreview) await page.deleteCookie(...gtmCookies);
  await page.close();
  return resumeMetrics(averageMetrics);
}

function resumeMetrics(metrics) {
  return {
    LayoutDuration: metrics.LayoutDuration,
    RecalcStyleDuration: metrics.RecalcStyleDuration,
    ScriptDuration: metrics.ScriptDuration,
    TaskDuration: metrics.TaskDuration,
    FirstPaint: metrics.FirstPaint,
    FirstMeaningfulPaint: metrics.FirstMeaningfulPaint,
    DOMContentLoaded: metrics.DOMContentLoadedChromeBlueLine,
    PageLoad: metrics.PageLoadChromeRedLine,
    Finish: metrics.Finish,
    Longtasks: metrics.longtasks
  };
}